---
theme: seriph
background: https://images.unsplash.com/photo-1618389041494-8fab89c3f22b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=987&q=80
class: "text-center"
highlighter: shiki
lineNumbers: false
info: "A talk about the future of Software development."
drawings:
  persist: false
download: true
---

# Future of software development

### Writing software for the next 50 years.

<div class="pt-12">
  <span @click="$slidev.nav.next" class="px-2 py-1 rounded cursor-pointer" hover="bg-white bg-opacity-10">
    Press Space for next page <carbon:arrow-right class="inline"/>
  </span>
</div>

<div class="abs-br m-6 flex gap-2">
  <a href="https://gitlab.com/avron/future-of-software-dev" target="_blank" alt="Source Code"
    class="text-xl icon-btn opacity-50 !border-none !hover:text-white">
    <carbon-code />
  </a>
</div>

<style>
* {
  font-family: monospace;
}
</style>

---

# Example One

Just the beginning.[^1]

```c {all|4|all}
#include <stdio.h>

int main() {
  if (true) {
    printf("Pever ⚡")
  } else {
    printf("Oh no ☠️")
  }
  return 0
}

```

[^1]: [Kinda fixed.](https://twitter.com/__phantomderp/status/1494884135688626180)

<style>
.footnotes-sep {
  @apply mt-20 opacity-10;
}
.footnotes {
  @apply text-sm opacity-75;
}
.footnote-backref {
  display: none;
}

* {
  font-family: monospace;
}
</style>

---

# Example Two

Imagine building a bank with Node.js.[^1]

```ts {all|1|2|4|all}
const numberA = 4324234234234231; // 16 digits long
const numberB = 4324234234234232; // 16 digits long

if (numberA === numberB) console.log("We'll live. 😇");
else console.log("We're so dead. 😬");
```

[^1]: [More Nightmare](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/BigInt#usage_recommendations)

<style>
.footnotes-sep {
  @apply mt-20 opacity-10;
}
.footnotes {
  @apply text-sm opacity-75;
}
.footnote-backref {
  display: none;
}

* {
  font-family: monospace;
}
</style>

---

# Example Three

<div class="flex w-full h-10/12 justify-center items-center">
<ul class="w-1/2">
<li> 🕰️  End of the world is on <b>03:14:08 Greenwich Mean Time (GMT, aka Coordinated Universal Time) January 19, 2038</b>.</li>
<li>Unless you update your operating system. ⏫</li>
</ul>
</div>

<style>
* {
  font-family: monospace;
}
</style>

---

# Software bad. Nightmare Bad.

<div class="flex w-full justify-center mt-12">
<img border="rounded" src="https://imgs.xkcd.com/comics/voting_machines.png">
</div>

<style>
* {
  font-family: monospace;
}
</style>

---

# What now?

<div class="flex w-full h-10/12 justify-center items-center">
<ul class="w-1/2">
  <li>✍🏽 Write better software.</li>
  <li>🫀 Slowly, gracefully replace old software.</li>
  <li>✨ Small wins matter!</li>
  <li><p>Good case studies:</p>
  <ul>
    <li>Eslint</li>
    <li>Erlang/OTP</li>
    <li>Zig Programming Language</li>
    <li>Rust Programming Language</li>
  </ul>
  </li>
</ul>
</div>

<style>
* {
  font-family: monospace;
}
</style>

---

# Thank You

<div class="flex w-full h-10/12 justify-center items-center">
<ul class="w-1/2">
<li class="space-x-4 flex items-center"><carbon-code /><p>: gitlab.com/avron</p></li>
<li class="space-x-4 flex items-center"><carbon-email /> <p>: mail[at]avronr[dot]in</p></li>
<li class="space-x-4 flex items-center"><carbon-send-alt /> <p>: @avronr</p></li>
<li class="space-x-4 flex items-center"><h4>[m]</h4> <p>: @abraham_raji:matrix.org</p></li>
</ul>
</div>

<style>
* {
  font-family: monospace;
}
</style>
